package com.readytogrok.kelvin

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by frank.sullivan on 12/8/17.
 */

private val testMsg = "Monitor running test."
private val timeoutMsg = "Monitor timed out. Retrieving fallback."
private val fallbackReceivedMsg = "Fallback retrieved. Running test again."
private val fallbackFailedMsg = "Fallback failed."

fun <T> Completable.monitor(
        test: (T) -> Boolean,
        fallback: () -> Single<T>,
        stream: () -> Observable<T>,
        delay: Long = 20,
        unit: TimeUnit = TimeUnit.SECONDS): Completable {
    return compose {
        andThen(stream())
                .filter {
                    Timber.d(testMsg)
                    test(it)
                }
                .timeout(
                        delay,
                        unit,
                        Observable.defer {
                            fallback()
                                    .doOnSubscribe {
                                        Timber.d(timeoutMsg)
                                    }.map {
                                        Timber.d(fallbackReceivedMsg)
                                        if (test(it)) {
                                            it
                                        } else {
                                            throw RuntimeException(fallbackFailedMsg)
                                        }
                                    }.toObservable()
                        }
                )
                .firstOrError()
                .toCompletable()
    }
}