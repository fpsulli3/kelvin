package com.readytogrok.kelvin

import android.app.Application
import timber.log.Timber

/**
 * Created by Frank on 12/6/2017.
 */
class App: Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}