package com.readytogrok.kelvin.common.data

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okio.Buffer
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import java.io.IOException

/**
 * Created by Frank on 12/6/2017.
 */

private val client = OkHttpClient.Builder()
//                .addInterceptor { chain ->
//                    val req = chain.request()
//                    val res = chain.proceed(req)
//
//                    Timber.d("OkHttpClient HTTP Request: %s %s with %s, Response: %d, %s",
//                            req.method(),
//                            req.url().toString(),
//                            bodyToString(req),
//                            res.code(),
//                            res.body()?.string() ?: "")
//
//                    res
//                }
        .build()

var retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl("http://www.readytogrok.com:9000/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

private fun bodyToString(request: Request): String {
    return try {
        val copy = request.newBuilder().build()
        val buffer = Buffer()
        copy.body()?.let {
            it.writeTo(buffer)
            buffer.readUtf8()
        } ?: ""
    } catch (e: IOException) {
        "did not work"
    }
}