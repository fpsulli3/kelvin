package com.readytogrok.kelvin.common.data

import io.reactivex.Completable
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Frank on 12/6/2017.
 */
internal interface ThermTempApi {
    @PUT("therm-temp/{thermostatId}")
    fun putThermostatTemperature(
            @Path("thermostatId") thermostatId: Int,
            @Query("value") value: Double) : Completable
}