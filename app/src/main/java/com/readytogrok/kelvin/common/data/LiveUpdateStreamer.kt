package com.readytogrok.kelvin.common.data

import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber

/**
 * Created by Frank on 12/6/2017.
 */
object LiveUpdateStreamer {
    private val api = retrofit.create(ThermTempApi::class.java)

    private val temperatureStream: Observable<LiveUpdate> = Observable.interval(5, TimeUnit.SECONDS)
            .flatMap {
                val temperature = 20.0 + 4.0*Math.cos(it.toDouble())
                api.putThermostatTemperature(8, temperature)
                        .toSingleDefault(LiveUpdate( property = "temp", value = temperature))
                        .toObservable()
            }

    private val fakeLiveUpdateStream: PublishSubject<LiveUpdate> = PublishSubject.create()

    val stream = Observable.merge(temperatureStream, fakeLiveUpdateStream)

    fun fakeHeatSetPointLiveUpdate(heatSetPoint: Double, delay: Long, unit: TimeUnit) {
        Completable.timer(delay, unit)
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        { fakeLiveUpdateStream.onNext(LiveUpdate(property = "heatSetPoint", value = heatSetPoint)) },
                        Timber::e)
    }

    fun fakeCoolSetPointLiveUpdate(coolSetPoint: Double, delay: Long, unit: TimeUnit) {
        Completable.timer(delay, unit)
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        { fakeLiveUpdateStream.onNext(LiveUpdate(property = "coolSetPoint", value = coolSetPoint)) },
                        Timber::e)
    }

    fun fakeModeLiveUpdate(mode: String, delay: Long, unit: TimeUnit) {
        Completable.timer(delay, unit)
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        { fakeLiveUpdateStream.onNext(LiveUpdate(property = "mode", mode = mode)) },
                        Timber::e)
    }

    data class LiveUpdate(
            val property: String,
            val value: Double? = null,
            val mode: String? = null)
}