package com.readytogrok.kelvin.thermostat.data.models

/**
 * Created by Frank on 12/6/2017.
 */
enum class ThermostatMode {
    Auto,
    Heat,
    Cool
}