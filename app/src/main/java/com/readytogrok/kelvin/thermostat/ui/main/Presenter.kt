package com.readytogrok.kelvin.thermostat.ui.main

import com.readytogrok.kelvin.thermostat.data.access.createProductionThermostatRepository
import com.readytogrok.kelvin.thermostat.data.models.ThermostatMode
import com.readytogrok.kelvin.thermostat.data.models.ThermostatModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by frank.sullivan on 12/14/17.
 */
internal class Presenter(
        thermostatId: Int,
        private val thermostatView: ThermostatView) {

    private val thermostatData = createProductionThermostatRepository(thermostatId)

    private val thermostatModelStream = thermostatData.getThermostatStream()

    val modeChanger = Observable.interval(6, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.newThread())
            .map { when (it % 3) {
                0L -> LayoutConfiguration.AUTO
                1L -> LayoutConfiguration.HEAT
                2L -> LayoutConfiguration.COOL
                else -> LayoutConfiguration.AUTO
            }}

    val viewModelStream: Observable<ViewModel>
            = thermostatModelStream
            .map {
                ViewModel(
                        mapModeToLayoutConfiguration(it),
                        calculateBackgroundColor(
                                it.temp,
                                it.heatSetPoint,
                                it.coolSetPoint,
                                it.mode),
                        formatTemperatureString(it.temp),
                        it.mode)
            }

    val coolSetPointStream: Observable<String>
            = thermostatModelStream
            .map { formatTemperatureString(it.coolSetPoint) }

    val heatSetPointStream: Observable<String>
            = thermostatData.getThermostatStream()
            .map { formatTemperatureString(it.heatSetPoint) }

    private fun formatTemperatureString(temperature: Double) =
            String.format("%.0f", temperature)

    private fun mapModeToLayoutConfiguration(it: ThermostatModel): LayoutConfiguration {
        return when (it.mode) {
            ThermostatMode.Auto -> LayoutConfiguration.AUTO
            ThermostatMode.Heat -> LayoutConfiguration.HEAT
            ThermostatMode.Cool -> LayoutConfiguration.COOL
        }
    }

    private fun calculateBackgroundColor(
            currentTemperature: Double,
            heatSetPoint: Double,
            coolSetPoint: Double,
            mode: ThermostatMode): BackgroundColor {
        return when (mode) {
            ThermostatMode.Auto -> calculateBackgroundColorAuto(currentTemperature, heatSetPoint, coolSetPoint)
            ThermostatMode.Heat -> calculateBackgroundColorHeat(currentTemperature, heatSetPoint)
            ThermostatMode.Cool -> calculateBackgroundColorCool(currentTemperature, coolSetPoint)
        }
    }

    private fun calculateBackgroundColorAuto(
            currentTemperature: Double,
            heatSetPoint: Double,
            coolSetPoint: Double): BackgroundColor {
        return when {
            currentTemperature in heatSetPoint..coolSetPoint -> BackgroundColor.IDLE
            currentTemperature < heatSetPoint -> BackgroundColor.HEATING
            else -> BackgroundColor.COOLING
        }
    }

    private fun calculateBackgroundColorHeat(
            currentTemperature: Double,
            heatSetPoint: Double): BackgroundColor {
        return if (currentTemperature >= heatSetPoint) {
            BackgroundColor.IDLE
        } else {
            BackgroundColor.HEATING
        }
    }

    private fun calculateBackgroundColorCool(
            currentTemperature: Double,
            coolSetPoint: Double): BackgroundColor {
        return if (currentTemperature <= coolSetPoint) {
            BackgroundColor.IDLE
        } else {
            BackgroundColor.COOLING
        }
    }
}