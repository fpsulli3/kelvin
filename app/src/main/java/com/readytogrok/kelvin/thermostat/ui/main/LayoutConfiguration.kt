package com.readytogrok.kelvin.thermostat.ui.main

/**
 * Created by frank.sullivan on 12/14/17.
 */
internal enum class LayoutConfiguration {
    HEAT,
    COOL,
    AUTO}