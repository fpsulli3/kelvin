package com.readytogrok.kelvin.thermostat.data.access

import com.readytogrok.kelvin.thermostat.data.models.ThermostatMode
import com.readytogrok.kelvin.thermostat.data.models.ThermostatModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Created by Frank on 12/6/2017.
 */
interface ThermostatRepository {
    fun getThermostat(): Single<ThermostatModel>
    fun setName(name: String): Completable
    fun setMode(mode: ThermostatMode): Completable
    fun setHeatSetPoint(setPoint: Double): Completable
    fun setCoolSetPoint(setPoint: Double): Completable
    fun setHeatAndCoolSetPoints(
            heatSetPoint: Double,
            coolSetPoint: Double): Completable

    fun getThermostatStream(): Observable<ThermostatModel>
}