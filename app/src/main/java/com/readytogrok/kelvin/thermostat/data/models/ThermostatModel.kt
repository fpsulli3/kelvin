package com.readytogrok.kelvin.thermostat.data.models

/**
 * Created by Frank on 12/6/2017.
 */
data class ThermostatModel(
        val id: Int,
        val name: String,
        val temp: Double,
        val heatSetPoint: Double,
        val coolSetPoint: Double,
        val mode: ThermostatMode,
        val increment: Double,
        val minSeparation: Double)
