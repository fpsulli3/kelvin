package com.readytogrok.kelvin.thermostat.data

import com.readytogrok.kelvin.common.data.LiveUpdateStreamer
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * Created by frank.sullivan on 1/3/18.
 */
internal class ThermostatSource(liveUpdateStreamer: LiveUpdateStreamer) {
    private val validThermostatId = 8

    // Whenever a patchThermostat call is made, updates to
    // optimistic values (current only the "name" property) will be
    // passed into this subject.
    private val updateSubject: PublishSubject<LiveUpdateStreamer.LiveUpdate>
            = PublishSubject.create()

    // By merging the above updateSubject with the LiveUpdateStreamer
    // stream, this Observable will emit both types of events.
    private val updateStream = Observable.merge(
            liveUpdateStreamer.stream.filter { isThermostatUpdate(it) },
            updateSubject)

    // Whenever a full Thermostat DTO is set, it is passed into
    // this subject.
    private val fullDtoSubject: PublishSubject<ThermostatDto> = PublishSubject.create()

    // This observable will take the latest full model that was retrieved,
    // and apply updates to it from the updateStream, and emit the resulting
    // updated DTO
    private val thermostatStream = fullDtoSubject.switchMap {
        // switchMap is like flatMap, except that it will switch all
        // subscribers to the latest ObservableSource immediately, whereas
        // flatMap will let each Observable complete before moving on to
        // the next one.

        // The Observable I'm creating here with scan() will apply Live Updates
        // to the latest full DTO that was emitted by the fullDtoSubject.
        updateStream.scan(it, {dto, update -> applyUpdate(update, dto)})
    }

    fun getThermostatStream(id: Int): Observable<ThermostatDto> {
        if (id != validThermostatId) {
            return Observable.error(IllegalArgumentException("Invalid thermostat id: $id"))
        }

        return thermostatStream
    }

    fun setThermostat(id: Int, dto: ThermostatDto) {
        if (id != validThermostatId) {
            return
        }

        fullDtoSubject.onNext(dto)
    }

    /**
     * Executes the PATCH method via the REST Api for the thermostat.
     * This method will also immediately update the thermostatStream upon
     * a successful patching of any "optimistic" values.
     *
     * @param patch The patch that should be applied to the thermostat.
     *
     * @return A Completable that completes upon a successful result
     *         from the REST Api.
     */
    fun patchThermostat(patch: ThermostatDto) {
        patch.name?.let {
            updateSubject.onNext(LiveUpdateStreamer.LiveUpdate(property = "name", mode = it))
        }
    }

    fun fakeLiveUpdatePatch(patch: ThermostatDto) {
        patch.heatSetPoint?.let {
            LiveUpdateStreamer.fakeHeatSetPointLiveUpdate(it, 1560, TimeUnit.MILLISECONDS)
        }

        patch.coolSetPoint?.let {
            LiveUpdateStreamer.fakeCoolSetPointLiveUpdate(it, 1650, TimeUnit.MILLISECONDS)
        }

        patch.mode?.let {
            LiveUpdateStreamer.fakeModeLiveUpdate(it, 1700, TimeUnit.MILLISECONDS)
        }
    }

    /**
     * Applies a LiveUpdate to the specified Thermostat DTO.
     *
     * @param liveUpdate The LiveUpdate structure containing the new data.
     * @param oldData The ThermostatDto containing the old data
     *
     * @return A ThermostatDto that reflects the result of the LiveUpdate being applies to the old data.
     */
    private fun applyUpdate(liveUpdate: LiveUpdateStreamer.LiveUpdate, oldData: ThermostatDto): ThermostatDto {
        return when (liveUpdate.property) {
            "temp" -> oldData.copy(temp = liveUpdate.value)
            "heatSetPoint" -> oldData.copy(heatSetPoint = liveUpdate.value)
            "coolSetPoint" -> oldData.copy(coolSetPoint = liveUpdate.value)
            "mode" -> oldData.copy(mode = liveUpdate.mode)
            else -> oldData
        }
    }

    private fun isThermostatUpdate(liveUpdate: LiveUpdateStreamer.LiveUpdate): Boolean {
        // In this demo, all LiveUpdates are thermostat updates.
        // However, in the real world, you'll want your Live Update
        // Adapters to have a meaningful filter
        return true
    }
}