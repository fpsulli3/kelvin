package com.readytogrok.kelvin.thermostat.data.access

import com.readytogrok.kelvin.common.data.LiveUpdateStreamer
import com.readytogrok.kelvin.common.data.retrofit
import com.readytogrok.kelvin.thermostat.data.*

/**
 * Created by Frank on 12/6/2017.
 */
fun createProductionThermostatRepository(thermostatId: Int): ThermostatRepository {
    val restApi = retrofit.create(ThermostatApi::class.java)
    val source = ThermostatSource(LiveUpdateStreamer)
    return ThermostatMapper(thermostatId, source, restApi)
}