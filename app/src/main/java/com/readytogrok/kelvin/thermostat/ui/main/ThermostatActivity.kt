package com.readytogrok.kelvin.thermostat.ui.main

import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v7.app.AppCompatActivity
import android.transition.TransitionManager
import android.view.View
import com.readytogrok.kelvin.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class ThermostatActivity : AppCompatActivity(), ThermostatView {
    private val heatConstraintSet = ConstraintSet()
    private val coolConstraintSet = ConstraintSet()
    private val autoConstraintSet = ConstraintSet()

    private var layoutConfiguration = LayoutConfiguration.AUTO
    private var backgroundColor = BackgroundColor.IDLE

    private lateinit var presenter: Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        heatConstraintSet.clone(this, R.layout.activity_main_heat_only)
        coolConstraintSet.clone(this, R.layout.activity_main_cool_only)
        autoConstraintSet.clone(rootConstraintLayout)

        presenter = Presenter(8, this)
    }

    override fun onResume() {
        super.onResume()

        presenter.viewModelStream
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::syncWithViewModel,
                        Timber::e)

        presenter.heatSetPointStream
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(heatSetPoint::setText, Timber::e)

        presenter.coolSetPointStream
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(coolSetPoint::setText, Timber::e)

        presenter.modeChanger
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setLayoutConfiguration)
    }

    override fun getHeatSetPointEventStream(): Observable<SetPointEvent> {
        return Observable.empty()
    }

    override fun getCoolSetPointEventStream(): Observable<SetPointEvent> {
        return Observable.empty()
    }

    private fun syncWithViewModel(viewModel: ViewModel) {
        updateBackgroundColor(viewModel.backgroundColor)
        setLayoutConfiguration(viewModel.configuration)
        current_temperature.text = viewModel.currentTemperature
    }

    private fun setLayoutConfiguration(layoutConfiguration: LayoutConfiguration) {
        if (this.layoutConfiguration != layoutConfiguration) {
            this.layoutConfiguration = layoutConfiguration
            TransitionManager.beginDelayedTransition(rootConstraintLayout)
            when (layoutConfiguration) {
                LayoutConfiguration.AUTO -> autoConstraintSet.applyTo(rootConstraintLayout)
                LayoutConfiguration.HEAT -> heatConstraintSet.applyTo(rootConstraintLayout)
                LayoutConfiguration.COOL -> coolConstraintSet.applyTo(rootConstraintLayout)
            }

            updateHeatSetPointVisibility(layoutConfiguration)
            updateCoolSetPointVisibility(layoutConfiguration)
        }
    }

    private fun updateCoolSetPointVisibility(layoutConfiguration: LayoutConfiguration) {
        val coolVisibility = when (layoutConfiguration) {
            LayoutConfiguration.AUTO -> View.VISIBLE
            LayoutConfiguration.HEAT -> View.GONE
            LayoutConfiguration.COOL -> View.VISIBLE
        }

        cool_set_point_label.visibility = coolVisibility
        coolSetPoint.visibility = coolVisibility
        cool_dec.visibility = coolVisibility
        cool_inc.visibility = coolVisibility
    }

    private fun updateHeatSetPointVisibility(layoutConfiguration: LayoutConfiguration) {
        val heatVisibility = when (layoutConfiguration) {
            LayoutConfiguration.AUTO -> View.VISIBLE
            LayoutConfiguration.HEAT -> View.VISIBLE
            LayoutConfiguration.COOL -> View.GONE
        }

        heat_set_point_label.visibility = heatVisibility
        heatSetPoint.visibility = heatVisibility
        heat_dec.visibility = heatVisibility
        heat_inc.visibility = heatVisibility
    }

    private fun updateBackgroundColor(backgroundColor: BackgroundColor) {
        if (this.backgroundColor != backgroundColor) {
            val anim = ObjectAnimator.ofArgb(
                    rootConstraintLayout,
                    "backgroundColor",
                    mapBackgroundColorToIntColor(this.backgroundColor),
                    mapBackgroundColorToIntColor(backgroundColor))

            this.backgroundColor = backgroundColor

            anim.start()
        }
    }

    private fun mapBackgroundColorToIntColor(backgroundColor: BackgroundColor): Int {
        return when (backgroundColor) {
            BackgroundColor.IDLE -> getColor(R.color.idle_bg)
            BackgroundColor.HEATING -> getColor(R.color.heating_bg)
            BackgroundColor.COOLING -> getColor(R.color.cooling_bg)
        }
    }
}
