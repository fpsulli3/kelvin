package com.readytogrok.kelvin.thermostat.ui.main

import com.readytogrok.kelvin.thermostat.data.models.ThermostatMode

/**
 * Created by frank.sullivan on 12/14/17.
 */
internal data class ViewModel(
        val configuration: LayoutConfiguration,
        val backgroundColor: BackgroundColor,
        val currentTemperature: String,
        val mode: ThermostatMode)