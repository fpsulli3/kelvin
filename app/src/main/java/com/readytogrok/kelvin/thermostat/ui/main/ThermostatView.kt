package com.readytogrok.kelvin.thermostat.ui.main

import io.reactivex.Observable

/**
 * Created by frank.sullivan on 12/14/17.
 */
internal interface ThermostatView {
    fun getHeatSetPointEventStream(): Observable<SetPointEvent>
    fun getCoolSetPointEventStream(): Observable<SetPointEvent>
}