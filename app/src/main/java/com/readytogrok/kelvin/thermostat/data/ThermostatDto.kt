package com.readytogrok.kelvin.thermostat.data

/**
 * Created by Frank on 12/6/2017.
 */
internal data class ThermostatDto(
        var id: Int? = null,
        var name: String? = null,
        var temp: Double? = null,
        var heatSetPoint: Double? = null,
        var coolSetPoint: Double? = null,
        var mode: String? = null,
        var increment: Double? = null,
        var minSeparation: Double? = null)