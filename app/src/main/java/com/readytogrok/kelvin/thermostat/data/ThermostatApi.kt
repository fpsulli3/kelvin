package com.readytogrok.kelvin.thermostat.data

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.Path

/**
 * Created by Frank on 12/6/2017.
 */
internal interface ThermostatApi {
    @GET("therm/{thermostatId}")
    fun getThermostat(@Path("thermostatId") thermostatId: Int): Single<ThermostatDto>

    @PATCH("therm/{thermostatId}")
    fun patchThermostat(
            @Path("thermostatId") thermostatId: Int,
            @Body body: ThermostatDto): Completable
}