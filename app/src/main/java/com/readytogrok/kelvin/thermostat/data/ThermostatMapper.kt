package com.readytogrok.kelvin.thermostat.data

import com.readytogrok.kelvin.monitor
import com.readytogrok.kelvin.thermostat.data.access.ThermostatRepository
import com.readytogrok.kelvin.thermostat.data.models.ThermostatMode
import com.readytogrok.kelvin.thermostat.data.models.ThermostatModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.atomic.AtomicBoolean

/**
 * The ThermostatMapper is meant to wrap the Thermostat Rest Api. Its purpose is
 * simply to map the Thermostat DTOs returned by the Rest Api to a
 * proper ThermostatModel.
 */
internal class ThermostatMapper(
        private val thermostatId: Int,
        private val source: ThermostatSource,
        private val api: ThermostatApi): ThermostatRepository {

    private val initialFetch = AtomicBoolean(false)

    override fun getThermostat(): Single<ThermostatModel> {
        return api.getThermostat(thermostatId)
                .subscribeOn(Schedulers.io())
                .doOnSuccess { source.setThermostat(thermostatId, it) }
                .map { dto -> mapDtoToModel(dto) }
    }

    override fun setName(name: String): Completable {
        val patch = ThermostatDto(name = name)
        return api.patchThermostat(thermostatId, patch)
                .subscribeOn(Schedulers.io())
                .doOnComplete { source.patchThermostat(patch) }
    }

    override fun setMode(mode: ThermostatMode): Completable {
        val patch = ThermostatDto(mode = mode.toString())
        return api.patchThermostat(thermostatId, patch)
                .subscribeOn(Schedulers.io())
                .doOnComplete { source.fakeLiveUpdatePatch(patch) }
                .monitor(
                        { it.mode == mode },
                        { getThermostat() },
                        { getThermostatStream() })
    }

    override fun setHeatSetPoint(setPoint: Double): Completable {
        val patch = ThermostatDto(heatSetPoint = setPoint)
        return api.patchThermostat(thermostatId, patch)
                .subscribeOn(Schedulers.io())
                .doOnComplete { source.fakeLiveUpdatePatch(patch) }
                .monitor(
                        { it.heatSetPoint == setPoint },
                        { getThermostat() },
                        { getThermostatStream() })
    }

    override fun setCoolSetPoint(setPoint: Double): Completable {
        val patch = ThermostatDto(coolSetPoint = setPoint)
        return api.patchThermostat(thermostatId, patch)
                .subscribeOn(Schedulers.io())
                .doOnComplete { source.fakeLiveUpdatePatch(patch) }
                .monitor(
                        { it.coolSetPoint == setPoint },
                        { getThermostat() },
                        { getThermostatStream() })
    }

    override fun setHeatAndCoolSetPoints(
            heatSetPoint: Double,
            coolSetPoint: Double): Completable {
        val patch = ThermostatDto(heatSetPoint = heatSetPoint, coolSetPoint = coolSetPoint)
        return api.patchThermostat(thermostatId, patch)
                .subscribeOn(Schedulers.io())
                .doOnComplete { source.fakeLiveUpdatePatch(patch) }
                .monitor(
                        { it.heatSetPoint == heatSetPoint && it.coolSetPoint == coolSetPoint },
                        { getThermostat() },
                        { getThermostatStream() })
    }

    override fun getThermostatStream(): Observable<ThermostatModel> {
        if (initialFetch.compareAndSet(false, true)) {
            getThermostat().subscribe()
        }

        return source.getThermostatStream(thermostatId)
                .map { mapDtoToModel(it) }
    }

    private fun mapDtoToModel(dto: ThermostatDto): ThermostatModel {
        return ThermostatModel(
                dto.id!!,
                dto.name!!,
                dto.temp!!,
                dto.heatSetPoint!!,
                dto.coolSetPoint!!,
                getModeFromString(dto.mode!!),
                dto.increment!!,
                dto.minSeparation!!)
    }

    private fun getModeFromString(modeStr: String): ThermostatMode {
        return when (modeStr) {
            "Auto" -> ThermostatMode.Auto
            "Heat" -> ThermostatMode.Heat
            "Cool" -> ThermostatMode.Cool
            else -> throw IllegalArgumentException("String provided is not a valid mode.")
        }
    }
}